<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('cierres', 'CierresController');
Route::resource('citas', 'CitasController');
Route::resource('clientes', 'ClientesController');
Route::resource('cotizaciones', 'CotizacionesController');
Route::resource('empleados', 'EmpleadosController');
Route::resource('eventos', 'EventosController');
Route::resource('eventosclientes', 'EventosClientesController');
Route::resource('fotoseventos', 'FotosEventosController');
Route::resource('llamadas', 'LlamadasController');
Route::resource('pagos', 'PagosController');
Route::resource('productos', 'ProductosController');
Route::resource('roles', 'RolesController');
Route::resource('sucursales', 'SucursalesController');
Route::resource('tiposcitas', 'TiposCitasController');
Route::resource('tiposllamadas', 'TiposLlamadasController');
Route::resource('usuarios', 'UsuariosController');

Route::post('usuarios/{id}/upload/avatar', 'UsuariosController@uploadAvatar');
Route::post('eventos/{id}/upload/avatar', 'EventosController@uploadAvatar');
Route::post('eventos/{id}/upload/picture/{id2}', 'FotosEventosController@uploadPicture');
Route::post('productos/{id}/upload/avatar', 'ProductosController@uploadAvatar');
Route::post('usuarios/{id}/changepassword', 'UsuariosController@changePassword');
Route::post('usuarios/password/reset', 'UsuariosController@recoveryPassword');

Route::post('login', 'AuthenticateController@login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
