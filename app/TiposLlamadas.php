<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TiposLlamadas extends Model
{
    use SoftDeletes;
    protected $table = 'tipos_llamadas';
}
