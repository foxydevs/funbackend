<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Cierres;
use Response;
use Validator;

class CierresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Response::json(Cierres::all(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'empleado'       => 'required',
            'cliente'        => 'required',
            'producto'       => 'required',
            'cuota'          => 'required',
            'total'          => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Cierres();
                $newObject->descripcion       = $request->get('descripcion');
                $newObject->produccion        = $request->get('produccion');
                $newObject->comision          = $request->get('comision');
                $newObject->cuota             = $request->get('cuota');
                $newObject->total             = $request->get('total');
                $newObject->descuento         = $request->get('descuento');
                $newObject->empleado          = $request->get('empleado');
                $newObject->cliente           = $request->get('cliente');
                $newObject->cita              = $request->get('cita');
                $newObject->vieneDe           = $request->get('vieneDe');
                $newObject->producto          = $request->get('producto');
                $newObject->save();
                return Response::json($newObject, 200);
            
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Cierres::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Cierres::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->descripcion       = $request->get('descripcion', $objectUpdate->descripcion);
                $objectUpdate->produccion        = $request->get('produccion', $objectUpdate->produccion);
                $objectUpdate->comision          = $request->get('comision', $objectUpdate->comision);
                $objectUpdate->cuota             = $request->get('cuota', $objectUpdate->cuota);
                $objectUpdate->total             = $request->get('total', $objectUpdate->total);
                $objectUpdate->descuento         = $request->get('descuento', $objectUpdate->descuento);
                $objectUpdate->empleado          = $request->get('empleado', $objectUpdate->empleado);
                $objectUpdate->cliente           = $request->get('cliente', $objectUpdate->cliente);
                $objectUpdate->cita              = $request->get('cita', $objectUpdate->cita);
                $objectUpdate->vieneDe           = $request->get('vieneDe', $objectUpdate->vieneDe);
                $objectUpdate->producto          = $request->get('producto', $objectUpdate->producto);
                $objectUpdate->estado            = $request->get('estado', $objectUpdate->estado);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Cierres::find($id);
        if ($objectDelete) {
            try {
                Cierres::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
