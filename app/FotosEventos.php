<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FotosEventos extends Model
{
    use SoftDeletes;
    protected $table = 'fotos_eventos';
}
