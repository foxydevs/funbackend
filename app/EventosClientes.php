<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventosClientes extends Model
{
    use SoftDeletes;
    protected $table = 'eventos_clientes';
}
