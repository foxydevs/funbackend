<?php

use Illuminate\Database\Seeder;

class CitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_citas')->insert([
            "nombre"            => "Pendiente",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_citas')->insert([
            "nombre"            => "No quiso",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_citas')->insert([
            "nombre"            => "Cancelo",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_citas')->insert([
            "nombre"            => "Reagendada",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_citas')->insert([
            "nombre"            => "Planto",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_citas')->insert([
            "nombre"            => "Cierre",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('citas')->insert([
            "lugar"             => "McDonalds",
            "fecha"             => "2017-12-12",
            "hora"              => "13:30:00",
            "descripcion"       => "Cita programada",
            "empleado"          => 1,
            "cliente"           => 1,
            "tipo"              => 1,
            "cita"              => null,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('citas')->insert([
            "lugar"             => "Cafe",
            "fecha"             => "2017-12-10",
            "hora"              => "13:00:00",
            "descripcion"       => "Cita programada",
            "empleado"          => 2,
            "cliente"           => 1,
            "tipo"              => 2,
            "cita"              => null,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('citas')->insert([
            "lugar"             => "Oficinas",
            "fecha"             => "2017-12-14",
            "hora"              => "13:30:00",
            "descripcion"       => "Cita programada",
            "empleado"          => 1,
            "cliente"           => 1,
            "tipo"              => 4,
            "cita"              => null,
            "estado"            => 0,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('citas')->insert([
            "lugar"             => "Oficinas",
            "fecha"             => "2017-12-16",
            "hora"              => "13:30:00",
            "descripcion"       => "Cita programada",
            "empleado"          => 1,
            "cliente"           => 1,
            "tipo"              => 1,
            "cita"              => 3,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

    }
}
