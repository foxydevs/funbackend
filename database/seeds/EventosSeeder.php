<?php

use Illuminate\Database\Seeder;

class EventosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eventos')->insert([
            "nombre"            => "Funeral de x Persona",
            "descripcion"       => "se hara el funeral de x persona",
            "fecha"             => "2017-12-08",
            "hora"              => "17:30:00",
            "lugar"             => "Capilla 1",
            "direccion"         => "Guatemala",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('eventos_clientes')->insert([
            "evento"            => 1,
            "cliente"           => 1,
            "estado"            => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('fotos_eventos')->insert([
            "foto"              => "http://clv.h-cdn.co/assets/17/24/980x653/funeral-etiquette-rude-behavior.jpg",
            "evento"            => 1,
            "usuario"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
    }
}
