<?php

use Illuminate\Database\Seeder;

class LlamadasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipos_llamadas')->insert([
            "nombre"            => "Respondida",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_llamadas')->insert([
            "nombre"            => "No Contesto",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_llamadas')->insert([
            "nombre"            => "No Se Intereso",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_llamadas')->insert([
            "nombre"            => "Llamar Mañana",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('tipos_llamadas')->insert([
            "nombre"            => "Ignorada",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('llamadas')->insert([
            "comentario"        => "Autorizado",
            "empleado"          => 1,
            "cliente"           => 1,
            "tipo"              => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('llamadas')->insert([
            "comentario"        => "No le intereso comprar",
            "empleado"          => 2,
            "cliente"           => 1,
            "tipo"              => 3,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
    }
}
