<?php

use Illuminate\Database\Seeder;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            "nombre"            => "Capilla Primium",
            "descripcion"       => "Capilla con todos los servicios",
            "precio"            => 10000,
            "descuento"         => 10,
            "sucursal"          => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('productos')->insert([
            "nombre"            => "Servicio Primium",
            "descripcion"       => "Capilla con todos los servicios",
            "precio"            => 50000,
            "descuento"         => 10,
            "sucursal"          => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('productos')->insert([
            "nombre"            => "Servicio General",
            "descripcion"       => "Capilla con servicios basicos",
            "precio"            => 20000,
            "descuento"         => 10,
            "sucursal"          => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('cotizaciones')->insert([
            "descripcion"       => "Capilla con servicios basicos",
            "producto"          => 3,
            "cliente"           => 1,
            "empleado"          => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('cotizaciones')->insert([
            "descripcion"       => "Capilla con servicios primium",
            "producto"          => 1,
            "cliente"           => 1,
            "empleado"          => 2,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
    }
}
