<?php

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            "nombre"            => "Administrador",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('roles')->insert([
            "nombre"            => "Usuario",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('roles')->insert([
            "nombre"            => "Vendedor",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            "username"          => "admin",
            "password"          => bcrypt('foxylabs'),
            "email"             => "admin@foxylabs.gt",
            "rol"               => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('empleados')->insert([
            "nombre"            => "Daniel",
            "apellido"          => "Rodriguez",
            "telefono"          => "54646431",
            "direccion"         => "Mazatenango",
            "fecha_nacimiento"  => "1995-01-06",
            "produccion"        => 0,
            "comision"          => 0,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            "username"          => "daniel",
            "password"          => bcrypt('foxylabs'),
            "email"             => "daniel@foxylabs.gt",
            "rol"               => 3,
            "empleado"          => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        
        DB::table('empleados')->insert([
            "nombre"            => "Alejandro",
            "apellido"          => "Godoy",
            "telefono"          => "54589531",
            "direccion"         => "Mazatenango",
            "fecha_nacimiento"  => "1993-03-27",
            "produccion"        => 0,
            "comision"          => 0,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            "username"          => "alejandro",
            "password"          => bcrypt('foxylabs'),
            "email"             => "alejandro@foxylabs.gt",
            "rol"               => 1,
            "empleado"          => 2,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
        
        DB::table('clientes')->insert([
            "nombre"            => "Jaime",
            "apellido"          => "Palma",
            "telefono"          => "78958548",
            "celular"           => "45568548",
            "direccion"         => "Mazatenango",
            "fecha_nacimiento"  => "1993-11-25",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('usuarios')->insert([
            "username"          => "jaime",
            "password"          => bcrypt('foxylabs'),
            "email"             => "ventas@foxylabs.gt",
            "rol"               => 2,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
    }
}
