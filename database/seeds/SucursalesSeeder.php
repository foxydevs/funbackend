<?php

use Illuminate\Database\Seeder;

class SucursalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sucursales')->insert([
            "nombre"            => "Guatemala",
            "descripcion"       => "Sucursal de Guatemala",
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
    }
}
