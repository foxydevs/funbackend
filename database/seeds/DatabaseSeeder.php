<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(SucursalesSeeder::class);
        $this->call(UsuariosSeeder::class);
        $this->call(LlamadasSeeder::class);
        $this->call(CitasSeeder::class);
        $this->call(EventosSeeder::class);
        $this->call(ProductosSeeder::class);
        $this->call(CierresSeeder::class);
    }
}
