<?php

use Illuminate\Database\Seeder;

class CierresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cierres')->insert([
            "descripcion"       => "Trato cerrado",
            "produccion"        => 1000,
            "comision"          => 500,
            "cuota"             => 2000,
            "total"             => 24000,
            "descuento"         => 4000,
            "empleado"          => 1,
            "cliente"           => 1,
            "cita"              => 1,
            "producto"          => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 1",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-01-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 2",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-02-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 3",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-03-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 4",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-04-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 5",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-05-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 6",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-06-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 7",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-07-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 8",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-08-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 9",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-09-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 10",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-10-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 11",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-11-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);

        DB::table('pagos')->insert([
            "descripcion"       => "Pago 12",
            "monto"             => 2000,
            "pagado"            => 0,
            "fecha"             => "2018-12-13",
            "cierre"            => 1,
            "cliente"           => 1,
            "created_at"        => date('Y-m-d H:m:s'),
            "updated_at"        => date('Y-m-d H:m:s')
        ]);
    }
}
