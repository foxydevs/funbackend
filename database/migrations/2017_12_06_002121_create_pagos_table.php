<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable()->default(null);
            $table->double('monto',15,2)->nullable()->default(null);
            $table->double('pagado',15,2)->nullable()->default(null);
            $table->timestamp('fecha_auth')->useCurrent();
            $table->date('fecha')->nullable()->default(null);
            $table->tinyInteger('estado')->default(1);
            
            $table->integer('cierre')->unsigned()->nullable()->default(null);
            $table->foreign('cierre')->references('id')->on('cierres')->onDelete('cascade');
            $table->integer('cliente')->unsigned()->nullable()->default(null);
            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
