<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCierresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cierres', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('fecha')->useCurrent();
            $table->string('descripcion')->nullable()->default(null);
            $table->double('produccion',15,2)->nullable()->default(null);
            $table->double('comision',15,2)->nullable()->default(null);
            $table->double('cuota',15,2)->nullable()->default(null);
            $table->double('total',15,2)->nullable()->default(null);
            $table->double('descuento',15,2)->nullable()->default(null);
            $table->tinyInteger('estado')->default(1);
            
            $table->integer('empleado')->unsigned()->nullable()->default(null);
            $table->foreign('empleado')->references('id')->on('empleados')->onDelete('cascade');
            $table->integer('cliente')->unsigned()->nullable()->default(null);
            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');
            $table->integer('cita')->unsigned()->nullable()->default(null);
            $table->foreign('cita')->references('id')->on('citas')->onDelete('cascade');
            $table->integer('vieneDe')->unsigned()->nullable()->default(null);
            $table->foreign('vieneDe')->references('id')->on('cierres')->onDelete('cascade');
            $table->integer('producto')->unsigned()->nullable()->default(null);
            $table->foreign('producto')->references('id')->on('productos')->onDelete('cascade');
            
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cierres');
    }
}
